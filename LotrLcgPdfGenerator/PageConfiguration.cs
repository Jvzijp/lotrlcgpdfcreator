﻿using iTextSharp.text;
using System.Drawing.Printing;

namespace LotrLcgPdfGenerator
{
    internal class PageConfiguration
    {
        public int Margins { get; }

        public Rectangle PageSize { get; }

        public int NumberOnPage => NumberOnLine * NumberLinesOnPage;
        public int NumberOnLine => (int)((PageSize.Width - 2 * Margins) / (ImageCellWidth));
        public int NumberLinesOnPage => (int)((PageSize.Height - 2 * Margins) / (ImageCellHeight));

        public float InnerMarginsHorizontal => NumberOnLine < 2 ? 0 :(PageSize.Width - 2 * Margins - NumberOnLine * ImageCellWidth) / (NumberOnLine - 1);
        public float InnerMarginsVertical => NumberLinesOnPage < 2 ? 0 : (PageSize.Height - 2 * Margins - NumberLinesOnPage * ImageCellHeight) / (NumberLinesOnPage - 1);

        public int MarkersMarginX { get; }
        public int MarkersMarginY { get; }
        public int MarkersOffsetX { get; }
        public int MarkersOffsetY { get; }
        public int MarkersSize { get; }

        public int ImageCellWidth { get; }
        public int ImageCellHeight { get; }

        public int ResolutionFactor { get; }

        public bool CreateCardBacks { get; }

        public string PlayerBackPath { get; }
        public string EncounterBackPath { get; }

        public PageConfiguration(
            float width, 
            float height, 
            int margins, 
            int imageCellWidth, 
            int imageCellHeight, 
            int markersMarginX, 
            int markersMarginY, 
            int markersOffsetX, 
            int markersOffsetY, 
            int markersSize, 
            int resolutionFactor, 
            bool createCardBacks,
            string playerBackPath, 
            string encounterBackPath)
        {
            PageSize = new Rectangle(width, height);
            Margins = margins;
            ImageCellWidth = imageCellWidth;
            ImageCellHeight = imageCellHeight;
            MarkersMarginX = markersMarginX;
            MarkersMarginY = markersMarginY;
            MarkersOffsetX = markersOffsetX;
            MarkersOffsetY = markersOffsetY;
            MarkersSize = markersSize;
            ResolutionFactor = resolutionFactor;
            CreateCardBacks = createCardBacks;
            PlayerBackPath = playerBackPath;
            EncounterBackPath = encounterBackPath;
        }
    }
}
