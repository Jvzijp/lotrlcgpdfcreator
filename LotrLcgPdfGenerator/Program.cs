﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using LotrLcgPdfGenerator;
using System.Xml;

internal class Program
{
    static iTextSharp.text.Document doc;



    static PdfWriter pdfWriter;


    static PageConfiguration? PageConfiguration;
    static MarkerDrawer MarkerDrawer;
    static CardListCreator CardListCreator;
    static ImageCellsCreator ImageCellsCreator;

    private static void Main(string[] args)
    {
        // Dependencies
        MarkerDrawer = new MarkerDrawer();
        CardListCreator = new CardListCreator();
        ImageCellsCreator = new ImageCellsCreator(MarkerDrawer);

        var isValid = true;

        if (args.Length != 17)
        {
            Console.WriteLine($"Incorrect number of parameters {args.Length}, should be 17");
            isValid = false;
        }

        var pdfFileName = args[0];
        if (!pdfFileName.Contains(".pdf"))
        {
            Console.WriteLine($"Arg 1: Output file does not contain .pdf  {pdfFileName}");
            isValid = false;
        }

        var inputXml = args[1];
        if (!inputXml.Contains(".xml"))
        {
            Console.WriteLine($"Arg 2: Iput file does not contain .xml  {inputXml}");
            isValid = false;
        }

        if (!File.Exists(inputXml)) 
        {
            Console.WriteLine($"Arg 3: Input file not found: {inputXml}");
            isValid = false;
        }

        var rootFolder = args[2];

        string value;
        value = args[3];
        if (!float.TryParse(value, out var pageWidth))
        {
            Console.WriteLine($"Arg 4: Page Width has incorrect format: {args[3]}");
            isValid = false;
        }

        value = args[4];
        if (!float.TryParse(value, out var pageHeight))
        {
            Console.WriteLine($"Arg 5: Page Height has incorrect format: {args[4]}");
            isValid = false;
        }

        value = args[5];
        if (!int.TryParse(value, out var pageMargins))
        {
            Console.WriteLine($"Arg 6: Page margins has incorrect format: {args[5]}");
            isValid = false;
        }

        value = args[6];
        if (!int.TryParse(value, out var cardWidth))
        {
            Console.WriteLine($"Arg 7: Card width has incorrect format: {args[6]}");
            isValid = false;
        }

        value = args[7];
        if (!int.TryParse(value, out var cardHeight))
        {
            Console.WriteLine($"Arg 8: Card height has incorrect format: {args[7]}");
            isValid = false;
        }

        value = args[8];
        if (!int.TryParse(value, out var markersMarginX))
        {
            Console.WriteLine($"Arg 9: Markers Margin Horizontal has incorrect format: {args[8]}");
            isValid = false;
        }

        value = args[9];
        if (!int.TryParse(value, out var markersMarginY))
        {
            Console.WriteLine($"Arg 10: Markers Margin Vertical has incorrect format: {args[9]}");
            isValid = false;
        }

        value = args[10];
        if (!int.TryParse(value, out var markersOffsetX))
        {
            Console.WriteLine($"Arg 11: Markers Offset Horizontal has incorrect format: {args[10]}");
            isValid = false;
        }

        value = args[11];
        if (!int.TryParse(value, out var markersOffsetY))
        {
            Console.WriteLine($"Arg 12: Markers Offset Vertical has incorrect format: {args[11]}");
            isValid = false;
        }

        value = args[12];
        if (!int.TryParse(value, out var markerWidth))
        {
            Console.WriteLine($"Arg 13: Markers Width has incorrect format: {args[12]}");
            isValid = false;
        }

        value = args[13];
        if (!int.TryParse(value, out var resolutionFactor))
        {
            Console.WriteLine($"Arg 14: Image resolution has incorrect format: {args[13]}");
            isValid = false;
        }

        value = args[14];
        if (!int.TryParse(value, out var renderBacks))
        {
            Console.WriteLine($"Arg 15: Render card backs has incorrect format: {args[14]}");
            isValid = false;
        }

        var playerBack = args[15];
        var encounterBack = args[16];

        if (!isValid)
        {
            OutputHelp();
            return;
        }

        //pageConfiguration = new PageConfiguration(196f, 267f, 0, 196, 267, 0, 0, 0, 0, 0, 600, true);
        //pageConfiguration = new PageConfiguration(595f, 842f, 0, 196, 267, 0, 0, 0, 0, 0, 200, true); //A4
        //PageConfiguration = new PageConfiguration(1191f, 842f, 15, 190, 260, 10, 5, 0, 0, 2, 200, true, @"D:\Lotr LCG\Enhanced Proxies\Card Backs\Original\cardback_player_upscaled_400.jpg", @"D:\Lotr LCG\Enhanced Proxies\Card Backs\Original\cardback_encounter_upscaled_400.jpg"); //A3 Landscape
        PageConfiguration = new PageConfiguration(pageWidth, pageHeight, pageMargins, cardWidth, cardHeight, markersMarginX, markersMarginY, markersOffsetX, markersOffsetY, markerWidth, resolutionFactor, renderBacks != 0, playerBack, encounterBack);



        if (PageConfiguration.NumberOnLine == 0 || PageConfiguration.NumberLinesOnPage == 0)
        {
            Console.WriteLine("Can not create grid");
            return;
        }
        Console.WriteLine($"Creating grid of {PageConfiguration.NumberOnLine} by {PageConfiguration.NumberLinesOnPage} cards");

        doc = new iTextSharp.text.Document(PageConfiguration.PageSize, PageConfiguration.Margins, PageConfiguration.Margins, PageConfiguration.Margins, PageConfiguration.Margins);

        CreatePdf(pdfFileName, inputXml, rootFolder);
    }

    private static void OutputHelp()
    {
        Console.WriteLine("usage: LotrLcgPdfGenerator.exe " +
            "   [output file (.pdf)] " +
            "   [input file (.xml)] " +
            "   [Page size horizontal] " +
            "   [Page size vertical] " +
            "   [Page margins] " +
            "   [Image cell width] " +
            "   [Image cell height] " +
            "   [Cutting Marker horizontal margin] " +
            "   [Cutting Marker vertical margin] " +
            "   [Cutting Marker horizontal offset] " +
            "   [Cutting Marker vertical offset] " +
            "   [Cutting Marker with] " +
            "   [Cutting Marker with] " +
            "   [Resolution scale] " +
            "   [Render card back (0 or 1)] " +
            "   [playerback location] " +
            "   [encounterback location]");
        Console.WriteLine("i.e. LotrLcgPdfGenerator.exe output.xml HuntForGollum.xml D:\\LotR\\Images");
        Console.WriteLine("Relative and absolute paths are supperted.");
    }

    private static void CreatePdf(string pdfFileName, string inputXml, string rootFolder)
    {
        try
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(inputXml);
            var allCards = CardListCreator.CreateCardList(xmlDocument, PageConfiguration);

            Console.WriteLine($"Creating file: {pdfFileName}");
            pdfWriter = PdfWriter.GetInstance(doc, new FileStream(pdfFileName, FileMode.Create));

            doc.Open(); 
            ImageCellsCreator.CreateImageCells(allCards, rootFolder, PageConfiguration, pdfWriter, doc);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
            //Log error;

        }
        finally
        {
            doc.Close();

        }
    }
}