﻿using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Drawing;

namespace LotrLcgPdfGenerator
{
    internal class ImageCellsCreator
    {
        static int numbersAddedToPage = 0;
        static int numbersAddedToLine = 0;
        static int lineIndex = 0;

        private readonly MarkerDrawer MarkerDrawer;

        public ImageCellsCreator(MarkerDrawer markerDrawer)
        {
            MarkerDrawer = markerDrawer;
        }

        public void CreateImageCells(IEnumerable<Card> cards, string rootFolder, PageConfiguration pageConfiguration, PdfWriter pdfWriter, Document doc)
        {
            lineIndex = 1;
            MarkerDrawer.DrawMarkers(pdfWriter, pageConfiguration);

            foreach (var card in cards)
            {
                if (numbersAddedToPage == pageConfiguration.NumberOnPage)
                {
                    Console.WriteLine("Creating new page");

                    doc.NewPage();
                    MarkerDrawer.DrawMarkers(pdfWriter, pageConfiguration);

                    numbersAddedToPage = 0;
                    lineIndex = 1;
                    numbersAddedToLine = 0;
                }
                if (numbersAddedToLine == pageConfiguration.NumberOnLine)
                {
                    lineIndex++;
                    numbersAddedToLine = 0;
                }

                if (!card.IsEmpty)
                {
                    CreateImageCell(card, rootFolder, pageConfiguration, pdfWriter);
                }

                numbersAddedToPage++;
                numbersAddedToLine++;
            }
        }

        public static void CreateImageCell(Card card, string rootFolder, PageConfiguration pageConfiguration, PdfWriter pdfWriter)
        {
            Console.WriteLine($"Adding {card.Filename}");

            var bitmap = Bitmap.FromFile(Path.Combine(rootFolder, card.Filename));

            bitmap = DrawImageInTheCenter(bitmap, pageConfiguration.ImageCellWidth * pageConfiguration.ResolutionFactor / 100, pageConfiguration.ImageCellHeight * pageConfiguration.ResolutionFactor / 100); //Rescale image


            iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(bitmap, BaseColor.MAGENTA);

            var xPosition = card.IsFront ? numbersAddedToLine : pageConfiguration.NumberOnLine - numbersAddedToLine - 1;

            img.ScaleToFit(new iTextSharp.text.Rectangle(pageConfiguration.ImageCellWidth, pageConfiguration.ImageCellHeight));
            img.SetAbsolutePosition(xPosition * pageConfiguration.ImageCellWidth + xPosition * pageConfiguration.InnerMarginsHorizontal + pageConfiguration.Margins, pageConfiguration.PageSize.Height - (lineIndex * pageConfiguration.ImageCellHeight + (lineIndex - 1) * pageConfiguration.InnerMarginsVertical) - pageConfiguration.Margins);
            pdfWriter.DirectContent.AddImage(img);
        }

        private static Bitmap DrawImageInTheCenter(System.Drawing.Image image, int width, int height)
        {
            var destRect = new System.Drawing.Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
                graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

                using (var wrapMode = new System.Drawing.Imaging.ImageAttributes())
                {
                    wrapMode.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, (int)(image.Width * 0.02), (int)(image.Height * 0.01), (int)(image.Width * 0.96), (int)(image.Height * 0.98), GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }
    }
}
