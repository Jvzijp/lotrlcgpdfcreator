﻿using iTextSharp.text.pdf;

namespace LotrLcgPdfGenerator
{
    internal class MarkerDrawer
    {
        public void DrawMarkers(PdfWriter pdfWriter, PageConfiguration pageConfiguration)
        {
            PdfContentByte cb = pdfWriter.DirectContent;
            cb.SetLineWidth(.3);

            float x1 = pageConfiguration.Margins + pageConfiguration.MarkersMarginX + pageConfiguration.MarkersOffsetX;
            float x2 = pageConfiguration.Margins + pageConfiguration.ImageCellWidth - pageConfiguration.MarkersMarginX + pageConfiguration.MarkersOffsetX;
            var markersPos = pageConfiguration.Margins - pageConfiguration.MarkersSize;

            while (x2 < pageConfiguration.PageSize.Width)
            {
                cb.MoveTo(x1, markersPos);
                cb.LineTo(x1, pageConfiguration.PageSize.Height - markersPos);
                cb.Stroke();

                cb.MoveTo(x2, markersPos);
                cb.LineTo(x2, pageConfiguration.PageSize.Height - markersPos);
                cb.Stroke();

                x1 += pageConfiguration.ImageCellWidth + pageConfiguration.InnerMarginsHorizontal;
                x2 += pageConfiguration.ImageCellWidth + pageConfiguration.InnerMarginsHorizontal;
            }

            var y1 = pageConfiguration.PageSize.Height - pageConfiguration.Margins - pageConfiguration.MarkersMarginY - pageConfiguration.MarkersOffsetY;
            var y2 = pageConfiguration.PageSize.Height - pageConfiguration.Margins - pageConfiguration.ImageCellHeight + pageConfiguration.MarkersMarginY - pageConfiguration.MarkersOffsetY;
            while (y2 > 0)
            {
                cb.MoveTo(markersPos, y1);
                cb.LineTo(pageConfiguration.PageSize.Width - markersPos, y1);
                cb.Stroke();

                cb.MoveTo(markersPos, y2);
                cb.LineTo(pageConfiguration.PageSize.Width - markersPos, y2);
                cb.Stroke();

                y1 -= pageConfiguration.ImageCellHeight + pageConfiguration.InnerMarginsVertical;
                y2 -= pageConfiguration.ImageCellHeight + pageConfiguration.InnerMarginsVertical;
            }
        }
    }
}
