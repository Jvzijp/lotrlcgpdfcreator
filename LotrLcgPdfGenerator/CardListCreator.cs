﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LotrLcgPdfGenerator
{
    internal class CardListCreator
    {
        static List<string> cardFronts = new List<string>();
        static List<string> cardBacks = new List<string>();

        public IEnumerable<Card> CreateCardList(XmlDocument xmlDocument, PageConfiguration pageConfiguration)
        {
            FillCardLists(xmlDocument, pageConfiguration);
            return CombineCardLists(pageConfiguration);
        }

        private static void FillCardLists(XmlDocument xmlDocument, PageConfiguration pageConfiguration)
        {
            bool useNextCardAsBack = false;

            foreach (XmlElement section in xmlDocument.SelectNodes("/deck/section"))
            {
                var folderPath = Path.Combine(section.Attributes["folder"].Value);
                Console.WriteLine($"Adding cards from folder {folderPath}");

                foreach (XmlElement card in section.ChildNodes)
                {
                    string filename = card.InnerText;
                    var path = Path.Combine(folderPath, filename);

                    var quantity = int.Parse(card.Attributes["qty"].Value);
                    var type = card.Attributes["type"].Value;

                    for (int i = 0; i < quantity; i++)
                    {
                        if (useNextCardAsBack)
                        {
                            cardBacks.Add(path);
                            useNextCardAsBack = false;
                            continue;
                        }
                        else
                        {
                            cardFronts.Add(path);
                        }

                        if (!pageConfiguration.CreateCardBacks) continue;

                        if (type.Equals("player", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(pageConfiguration.PlayerBackPath))
                        {
                            cardBacks.Add(pageConfiguration.PlayerBackPath);
                        }
                        else if (type.Equals("encounter", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(pageConfiguration.EncounterBackPath))
                        {
                            cardBacks.Add(pageConfiguration.EncounterBackPath);
                        }
                        else
                        {
                            useNextCardAsBack = true;
                        }
                    }
                }
            }
        }

        private static IEnumerable<Card> CombineCardLists(PageConfiguration pageConfiguration)
        {
            List<Card> frontCards = new List<Card>();
            List<Card> backCards = new List<Card>();
            List<Card> allCards = new List<Card>();

            int numberOfCardsTaken = 0;

            while (cardFronts.Any())
            {
                var numberToTake = pageConfiguration.NumberOnLine < cardFronts.Count ? pageConfiguration.NumberOnLine : cardFronts.Count;

                frontCards.AddRange(cardFronts.Take(numberToTake).Select(card => new Card(card, true)));
                cardFronts.RemoveRange(0, numberToTake);

                if (cardBacks.Any())
                {
                    backCards.AddRange(cardBacks.Take(numberToTake).Select(card => new Card(card, false)));
                    cardBacks.RemoveRange(0, numberToTake);
                }

                numberOfCardsTaken += numberToTake;
                if (numberOfCardsTaken == pageConfiguration.NumberOnPage)
                {
                    allCards.AddRange(frontCards);
                    allCards.AddRange(backCards);

                    frontCards.Clear();
                    backCards.Clear();
                    numberOfCardsTaken = 0;
                }
            }

            if (frontCards.Count != 0)
            {
                while (frontCards.Count < pageConfiguration.NumberOnPage)
                {
                    frontCards.Add(Card.EmptyCard);
                }
            }

            allCards.AddRange(frontCards);
            allCards.AddRange(backCards);

            frontCards.Clear();
            backCards.Clear();
            numberOfCardsTaken = 0;

            return allCards;
        }
    }
}
