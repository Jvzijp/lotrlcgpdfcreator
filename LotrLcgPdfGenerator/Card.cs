﻿namespace LotrLcgPdfGenerator
{
    internal class Card
    {
        public string Filename;
        public bool IsFront;
        public bool IsEmpty;
        private Card(bool isEmpty)
        {

            IsEmpty = isEmpty;
        }

        public Card(string filename, bool isFront)
        {
            Filename = filename;
            IsFront = isFront;
        }

        public static Card EmptyCard => new Card(true);

    }
}
