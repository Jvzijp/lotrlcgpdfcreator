Command line arguments:

PDF file to create (relative or absolute folder folder)
XML source file to take the card data from (relative or absolute folder folder)
Root folder of the image files
Width of the pages in 1/72 inch
Height of the pages in 1/72 inch
Page margin size in 1/72 inch
Width of the cards in 1/72 inch
Height of the cards in 1/72 inch
Horizontal margin of the cut marks in 1/72 inch
Vertical margin of the cut marks in 1/72 inch
Horizontal offset of the cut marks in 1/72 inch
Vertical offset of the cut marks in 1/72 inch
Length of the cut marks in 1/72 inch
Image quality factor (100=72dpi, 200=144dpi etc)
Whether to draw card backs on alternating page (0=no, 1=yes)
Image file of the card backs for player cards
Image file of the card backs for encounter cards